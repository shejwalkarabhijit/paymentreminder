<?php 
/**
   * Command line utility to get payment date reminder information in form of CSV format.
   * @Developer  Name: Abhijit Shejwalkar
*/

class PaymentReminder {

    protected $message;
    protected $arr_record = [];


    /**
     * Get payment information by passing the year
     * @param : string $year 2020
     * @return : get csv file with payment date information. 
     */
    public function information( $year ) {

        // validate the year input.  
        if( empty( $year ) || ! $this->isValidYear( $year ) )  {
              
            return $this->message =  "Please enter valid year to get payment related csv file";

          } //if
          
          $this->arr_record [] =   [ "Month", "Payment Date",  "Bonus Date" ];

          for( $intMonth = 01; $intMonth <= 12; $intMonth++ ) { 

            $startMonthDate = $year.'-'.$intMonth.'-01';
            $middleOfMonth =  $year.'-'.$intMonth.'-15';

            $payment_date = $this->getPaymentDate( $startMonthDate );  // get payment date
            $bonus_date = $this->getBonusDate( $middleOfMonth );  // get bonus date 
            $month_name = DateTime::createFromFormat('m', $intMonth)->format('F');
            $this->arr_record [] =   [ $month_name, $payment_date,  $bonus_date ];

        } //end for

        if( true == $this->exportToCsv( $this->arr_record, $year ) ) {

            $this->message = "CSV file get created successfully";

        } else {
            $this->message = "Error While creating the CSV file";
        }

         return $this->message;

    } //end information  

    /**
     * Get payment date. check the condition like 
     * Paid on the last day of the month unless that day is a Saturday or a Sunday (weekend).
     * @param : date $strDate 2020-01-01
     * @return : get payment date. 
     */

    public function getPaymentDate( $strDate ) {

        $lastdateofthemonth  = date("Y-m-t", strtotime($strDate)); 

        $lastworkingday = date('l', strtotime($lastdateofthemonth));

        if($lastworkingday == "Saturday") { 
        $newdate = strtotime ('-1 day', strtotime($lastdateofthemonth));
        $lastdateofthemonth = date ('Y-m-j', $newdate);
        }
        elseif($lastworkingday == "Sunday") { 
         $newdate = strtotime ('-2 day', strtotime($lastdateofthemonth));
         $lastdateofthemonth = date ( 'Y-m-j' , $newdate );
        } 

        return $lastdateofthemonth;
 
    } //end getPaymentDate


     /**
     * Get bonus date. check the condition like 
     * On the 15th of every month bonuses are paid for the previous month,
     * unless that day is a weekend. In that case, they are paid the first Wednesday after the 15th.
     * @param : date $strDate 2020-01-15
     * @return : get bonus date. 
     */

    public function getBonusDate( $strDate ) {

        if( date('l', strtotime($strDate)) == 'Sunday' || date('l', strtotime($strDate)) == 'Saturday')  {

            // Create a new DateTime object
            $date = new DateTime($strDate);
        
            // Modify the date it contains
            $date->modify('next wednesday');
        
            // Output
            return $date->format('Y-m-d');
        
        } else {
            return $strDate;
        }
 
    } // getBonusDate


     /**
     * Check validate year 
     * @param : string $year 2020-01-15
     * @return : true
     */
    public function isValidYear( $year ) {

        // Convert to timestamp
        $start_year         =   strtotime(date('Y') - 100); //100 Years back
        $received_year      =   strtotime($year);

        // Check that user date is between start & end
        return (($received_year >= $start_year) && ( strlen( $year )  == 4 ) );

    }

    /**
     * Create CSV report for payment date information
     * @param : array $row  
     * @param : string $year
     * @return : true created csv file. 
     */
    function exportToCsv( $row, $year ) {
           
        $file_name = time().'_Report_for_year_'.$year.'.csv';

        // Open a file in write mode ('w')
        $fp = fopen($file_name, 'w');
          
        // Loop through file pointer and a line
        foreach ($row as $fields) {
            fputcsv($fp, $fields);
        }
          
        fclose($fp);
        return true;
       
    } //end exportToCsv

}

//validate the argument 
if( count($argv) != 2 ) {
    echo "Please enter the Year paramater";exit;
}

$year = $argv[1];
$getResult = new PaymentReminder();
echo $getResult->information( $year );


?>